.. _getting_started:


***************
Getting started
***************

.. _opening:

Opening ScratchyCAD on Chrome
=============================

First go ahead and visit `ScratchyCAD <http://printmyrobot.com/mm3d/>`_
remember this is an early Alpha testing version expect the following to happen:

* Things might break
* Current version might loose compatibility with future versions
* Currently only Chrome and Chrominium web browsers are supported

If you find any bugs and want to help us out send us an email at::

  scratchycad@scratchycad.com

.. _blocks:

How blocks work
=============================

Every block has a small connector shaped as a puzzle, its important to figure
out the shape the form of the connector and what value it stores to be use it
with other blocks::

  some example shapes go here
